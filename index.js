const readlineSync = require('readline-sync');
//если windows, то ввести в терминале для русских букв: chcp 65001

const Monster = {
    maxHealth: 10,
    name: "Лютый",
    moves: [
        {
            "name": "Удар когтистой лапой",
            "physicalDmg": 3, // физический урон
            "magicDmg": 0,    // магический урон
            "physicArmorPercents": 20, // физическая броня
            "magicArmorPercents": 20,  // магическая броня
            "cooldown": 0     // ходов на восстановление
        },
        {
            "name": "Огненное дыхание",
            "physicalDmg": 0,
            "magicDmg": 4,
            "physicArmorPercents": 0,
            "magicArmorPercents": 0,
            "cooldown": 3
        },
        {
            "name": "Удар хвостом",
            "physicalDmg": 2,
            "magicDmg": 0,
            "physicArmorPercents": 50,
            "magicArmorPercents": 0,
            "cooldown": 2
        },
    ]
}




const Warlock = {
    maxHealth: 20,
    name: "Евстафий",
    moves: [
        {
            "name": "Удар боевым кадилом",
            "physicalDmg": 2,
            "magicDmg": 0,
            "physicArmorPercents": 0,
            "magicArmorPercents": 50,
            "cooldown": 0
        },
        {
            "name": "Вертушка левой пяткой",
            "physicalDmg": 4,
            "magicDmg": 0,
            "physicArmorPercents": 0,
            "magicArmorPercents": 0,
            "cooldown": 4
        },
        {
            "name": "Каноничный фаербол",
            "physicalDmg": 0,
            "magicDmg": 5,
            "physicArmorPercents": 0,
            "magicArmorPercents": 0,
            "cooldown": 3
        },
        {
            "name": "Магический блок",
            "physicalDmg": 0,
            "magicDmg": 0,
            "physicArmorPercents": 100,
            "magicArmorPercents": 100,
            "cooldown": 4
        },
    ]
}


class GameRPGbattle {

    constructor() {
      this.difficulty = this.getDifficulty();
      this.firstMoveComputer = true;  
      this.monster = Object.create(Monster); 
      this.warlock = Object.create(Warlock);
      this.warlock.health = this.difficulty;
      this.monster.health = this.monster.maxHealth;
      console.log(`Здоровье ${this.monster.name}: ${this.monster.health}`);
      console.log(`Здоровье ${this.warlock.name}: ${this.warlock.health} \n`);
      this.generateImgMoves();
      this.arr = [];
    }

    getDifficulty = () =>{
        let difficulty = readlineSync.question(`Выберите сложность игры (начальное здоровье Евстафия) - чем меньше, тем сложнее, число > 0 :`);
        if (isNaN(difficulty) || difficulty < 1 || Math.round(+difficulty)!==(+difficulty)){
            console.log(`Введите правильное число!`);
            difficulty = this.getDifficulty();
        }
        if(difficulty > 10000){
            console.log(`Легче некуда, 10000 будет достаточно для победы)`);
            difficulty = 10000;
        }
        return +difficulty;
    }

    generateImgMoves = () => {
        const arrImg = [
`███████████▀▀▀██████████████████████████
███████▀░░░▄█▄░░▄▀██████████████████████
██████░▄██░░▀░░██▀▀█████████████████████
█████▀░░░▄██████░░░░████████████████████
█████░██░████████▄▄█▀▀██████████████████
█████░░░░▀████████▄░░░▀█████████████████
█████▄░██░██████████░░░░████████████████
██████▄░░▄░▀█████████▄░░░▀██████████████
███████████░███▀░█████▄░░░░█████████████
███████░░▀█▄███▄▄███████░░░░▀███████████
███████▄░░░▀█░░░░▀███████▄░░░▀██████████
██▀▀▀███░░▄▄▀░░░░▄▄████████░░░░▀████████
███▄░█░▀▀▀▀░░░░░░█░░░░░▄▄███▄░░░▀███████
██████░░░░░░░░░░░█░░░▄███████▄░░░░██████
██████░░░░░░░░░░░░▀▀███████████░░░░▀████
███████░░░█▀▀▀▀▀▄░░▄████████████▄░░░████
██████░▀▄░█░░░░█░▄█▀░█████████████▄█▀░██
█████░▄▄████░░░██████▄█████████████▄▄▄██
█████████████░██████████████████████████
████████████████████████████████████████`,

`⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⢀⣠⣄⡀
⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⢀⣤⣾⣿⣿⠟⠋
⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⢀⣤⣾⣿⡿⠛⠉
⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⢀⣴⣾⣿⠿⠋⠁
⠄⠄⠄⠄⠄⠄⠄⢀⣴⣾⡿⠟⠋
⠄⠄⠄⡀⢾⣶⣾⡿⠛⠁
⠄⣴⣿⣷⡘⢿⡇⠄⣠⣤⣤⣦⡀
⠸⣿⣿⣿⠁⠄⢠⣾⣿⣿⣿⣿⣿⣧
⠄⣿⣿⣿⠄⠄⣿⣿⣿⣿⣿⣿⣿⡿
⢠⣿⣿⣿⡄⢰⣿⣿⣿⣧⣌⣉⣈⣡
⢈⣿⣿⣿⣷⣿⣿⣿⣿⣿⣿⣿⣿⠟⠄⠄⠄⠄⠄⠄⠄⣤⡀
⠄⢻⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⣶⣶⣶⣦⣀⠄⣼⣿⡇
⠄⠄⠉⠛⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣶⡦
⠄⠄⠄⠄⠸⣿⣿⣿⣿⣿⣿⣿⣿⡿⡿⣿⡿⠿⠛⠛⠛⠛⠁
⠄⠄⣀⡀⢶⣦⣙⠛⣛⡛⠿⠛⣩⣴
⢠⣿⣿⣿⣦⣙⠻⢿⣿⣿⣿⠿⢋⣡⣾⣶⡄
⠘⣿⣿⣿⣿⣿⠇⣼⡏⢿⡆⢺⣿⣿⣿⣿⣧
⠄⠈⣿⣿⣿⡇⠈⠛⠄⠘⠋⠄⢿⣿⣿⣿⣿⣦⣀⡀
⠰⣾⣿⣿⣿⠇⠄⠄⠄⠄⠄⠄⠄⠄⠙⠿⣿⣿⣿⣿⡇
⠄⠈⠛⠉⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠙⠛⠛⠛⠁`,  

`⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡿⠻⠿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
⣿⣿⣿⣿⣿⣿⣿⠿⠋⠉⠄⠄⠄⠄⠄⠈⠉⠉⢛⣿⣿⣿⣿⣿
⣿⣿⣿⣿⣿⡟⠁⠄⣠⣴⠿⠛⢻⡷⠶⢤⣤⣄⣸⣧⡍⢙⠻⣿
⣿⣿⣿⣿⠋⠄⢠⣾⠟⠛⠓⣶⢿⣤⣄⣸⣇⠉⢻⣿⠁⣿⠄⣿
⣿⣿⣿⠏⠄⣼⣿⡁⠄⠄⣰⠏⠄⠄⠉⢹⡿⠛⠿⢿⣇⣙⣠⣿
⣿⣿⡇⢀⣼⣿⠋⠛⠻⢶⡿⣤⣤⣀⡀⢸⡇⠄⠄⢸⣿⣿⣿⣿
⣿⣿⠄⣸⣿⣇⠄⠄⠄⣾⠄⠄⠈⠉⠛⣻⠳⠶⣦⣤⣿⣿⣿⣿
⣿⣿⣷⣿⣿⡏⠙⠛⢿⣿⣤⣄⣀⣀⢠⡿⠄⠄⠄⣸⣿⣿⣿⣿
⣿⣿⣿⣿⣿⡇⠄⠄⣿⠃⠄⠄⠉⠙⣿⠷⠶⢤⣤⣿⣿⣿⣿⣿
⣿⣿⣿⣿⣿⣿⡛⠲⣿⣦⣤⣀⡀⣼⠇⠄⠄⢠⣾⣿⣿⣿⣿⣿
⣿⣿⣿⣿⣿⣿⣷⣀⣿⡀⠈⢉⣽⠛⠻⢶⣶⣿⣿⣿⣿⣿⣿⣿
⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣶⣿⣥⣤⣶⣿⣿⣿⣿⣿⣿⣿⣿⣿`,

`⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡿⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠿⠛⠁⠄⠄⠈⠛⠿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
⣿⣿⣿⣿⣿⡿⠿⠛⠋⠁⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠉⠛⠻⢿⣿⣿⣿⣿⣿⣿
⣿⣿⠛⠉⠁⠄⠄⠄⠄⠄⠄⠄⠄⢀⣀⣀⡀⠄⠄⠄⠄⠄⠄⠄⠄⠉⠛⠻⣿⣿
⣿⣿⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⢸⣿⣿⡇⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⣿⣿
⣿⣿⡇⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⢸⣿⣿⡇⠄⠄⠄⠄⠄⠄⠄⠄⠄⠄⢰⣿⣿
⣿⣿⣿⠄⠄⠄⠄⢀⣀⣀⣀⣀⣀⣸⣿⣿⣇⣀⣀⣀⣀⣀⡀⠄⠄⠄⠄⣾⣿⣿
⣿⣿⣿⣧⠄⠄⠄⢸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡇⠄⠄⠄⣸⣿⣿⣿
⣿⣿⣿⣿⣆⠄⠄⠈⠉⠉⠉⠉⠉⢹⣿⣿⡏⠉⠉⠉⠉⠉⠁⠄⠄⣰⣿⣿⣿⣿
⣿⣿⣿⣿⣿⣦⠄⠄⠄⠄⠄⠄⠄⢸⣿⣿⡇⠄⠄⠄⠄⠄⠄⠄⣰⣿⣿⣿⣿⣿
⣿⣿⣿⣿⣿⣿⣷⡀⠄⠄⠄⠄⠄⢸⣿⣿⡇⠄⠄⠄⠄⠄⢀⣼⣿⣿⣿⣿⣿⣿
⣿⣿⣿⣿⣿⣿⣿⣿⣦⡀⠄⠄⠄⠈⠉⠉⠁⠄⠄⠄⠄⣴⣿⣿⣿⣿⣿⣿⣿⣿
⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⣄⡀⠄⠄⠄⠄⠄⠄⠄⣠⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿
⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣦⡀⠄⠄⢀⣴⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿`,
        ];
        for(let i=0; i < this.warlock.moves.length; i++){
            this.warlock.moves[i].img = arrImg[i];
        };
    }

    generateMove = () => {
        const movesCount = this.monster.moves.length;
        let rand = Math.random() * movesCount;
        if (!this.validateMoveCool(this.monster, Math.floor(rand))){
            rand = this.generateMove();
        }
        return Math.floor(rand);
    }

    getAnswer = () => {       
        let userMove = readlineSync.question(`Введите номер действия: `);
        if((userMove!=="exit") && (isNaN(userMove) || userMove < 1 || userMove > this.warlock.moves.length || Math.round(+userMove)!==(+userMove))){
            console.log(`Введите правильное число!`);
            userMove = this.getAnswer();
        }        
        if ((userMove!=="exit") && (!this.validateMoveCool(this.warlock, userMove-1))){
            console.log(`Это действие еще заморожено!`);
            userMove = this.getAnswer();
        }
        if(userMove === "exit"){
            return "exit";
        }
        return userMove;    
    }

    countHealth = () => {
        let user = this.warlock;
        let monster = this.monster;
        //console.log(user.move, " ",monster.move); //для тестирования
        user.health = user.health - ((100 - user.move.physicArmorPercents) * monster.move.physicalDmg /100);
        user.health = user.health - ((100 - user.move.magicArmorPercents) * monster.move.magicDmg /100);
        user.health = Math.floor(user.health);
        if(user.health < 0){
            user.health = 0;
        }
        monster.health = monster.health - ((100 - monster.move.physicArmorPercents) * user.move.physicalDmg /100);
        monster.health = monster.health - ((100 - monster.move.magicArmorPercents) * user.move.magicDmg /100);
        monster.health = Math.floor(monster.health);
        if(monster.health < 0){
            monster.health = 0;
        }
        return [user.health, monster.health];
    }

    validateMoveCool = (obj, idMove) => {           
        if(obj.moves[idMove].numberCool && obj.moves[idMove].numberCool !==0 ){
            return false;
        }
        return true;
    }

    countCooldown = (moveIdUser, moveIdMonster) => {
        this.monster.moves.forEach(function(current, i) {
            if(moveIdMonster !== i){
                if(current.numberCool && current.numberCool!==0){
                    current.numberCool -= 1;
                }
            }
            else{
                current.numberCool = current.cooldown;
            }
        });

        this.warlock.moves.forEach(function(current, i) {
            if(moveIdUser !== i){
                if(current.numberCool && current.numberCool!==0){
                    current.numberCool -= 1;
                }
            }
            else{
                current.numberCool = current.cooldown;
            }
        });

        //console.log(this.warlock.moves, " ",this.monster.moves); //для тестирования
        return [this.warlock.moves, this.monster.moves];
    }

    showMoveOptions = () => {
        for(let i=0; i < this.warlock.moves.length; i++){
            if (this.validateMoveCool(this.warlock, i)){
                console.log(`${i+1} - ${this.warlock.moves[i].name}`);
            }
        }
    }

    moveTotal = () => {      
        [this.warlock.health, this.monster.health] = this.countHealth();
        console.log('Ход окончен, итоги хода:');
        console.log(`Здоровье ${this.monster.name}: ${this.monster.health}`);
        console.log(`Здоровье ${this.warlock.name}: ${this.warlock.health} \n`);
        console.log('................................................ \n');
    }

    total = () => {  
        let userName = this.warlock.name;
        let monsterName = this.monster.name;
        console.log(`Ход игры: `);     
        this.arr.forEach(function(current, i) {
            console.log(`Ход ${i+1}: ${monsterName} - ${current[0]}, ${userName} - ${current[1]}, \n Здоровье: ${current[2]} | ${current[3]}`);
        });
    }

    start = () => {
        if(Math.min(this.warlock.health, this.monster.health) === 0) {
            if( this.warlock.health - this.monster.health > 0 ){
                console.log(`Ура, вы выиграли!!! \n`);
            }
            else if(this.warlock.health - this.monster.health < 0){
                console.log(`Увы, выиграл соперник:( Сыграем еще раз? \n`);
            }
            else if(this.warlock.health === this.monster.health){
                console.log(`Ничья, это был достойный бой! \n`);
            }
            this.total();
            return;
        } 

        let moveIdMonster = this.generateMove();     
        this.monster.move = this.monster.moves[moveIdMonster];       
        console.log(`${this.monster.name} собирается нанести удар: "${this.monster.move.name}" \n`);

        console.log(`Чем ответим?`);
        this.showMoveOptions();   
        let moveIdWarlock = this.getAnswer()-1; 
        if (moveIdWarlock === "exit"){
            console.log(`Вы вышли из игры, до встречи!`);
            this.total();
            return;
        }
        this.warlock.move = this.warlock.moves[moveIdWarlock];       
        console.log(`\n${this.warlock.name} собирается нанести удар: "${this.warlock.move.name}" \n`);       

        console.log(`Подождите немножко, идет бой!...`);
        console.log(this.warlock.move.img);

        setTimeout(() => {           
            this.moveTotal();
            this.arr.push([this.monster.move.name, this.warlock.move.name, this.monster.health, this.warlock.health]);   
            [this.warlock.moves, this.monster.moves] = this.countCooldown(moveIdWarlock, moveIdMonster);              
            this.start();
        }, 1000);       
    }
}

let game = new GameRPGbattle();
try{
    game.start(); 
} catch (err){
    console.log(`Извините, произошла непредвиденная ошибка: `, err.message);
}